# Lab5 -- Integration testing

## Classification Tree

![tree](images/classification_tree.jpg)

## BVA Table

![bva](images/BVA.png)

## Decision Table

![decision](images/decision_table.png)

## Test cases

[LINK](https://docs.google.com/spreadsheets/d/1p4D3Y1fOvRmQTE8ooC32GWIzZ3mQ-AfXZwJ9xe9ZtyA/edit?usp=sharing)

HTTP requests are here `example.http`

## Bugs

1. Invalid data does not lead to an error
1. Price calculation does not work correctly
